let users = Vue.component("users", {
    template: `
        <div class="container">
            <h1>Users</h1>
            <transition name="fade" mode="out-in" appear>
                <template v-if="usersExist() === true">
                        <table class="table table-stripped table-hover">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Name and surname</th>
                                    <th>Age</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <transition-group tag="tbody" name="fade">
                                <user-row v-for="(user, index) in users" v-bind:user="user" v-bind:index="index" v-bind:key="user.id"></user-row>
                            </transition-group>
                        </table>
                    
                </template>
                <template v-else>
                    <div class="toast">No users to display</div>
                </template>
            </transition>
        </div>
    `,
    props: ["users"],
    methods: {
        usersExist() {
            return this.users.length >= 1;
        }
    },
    components: {
        'user-row': userRow,
    },
});