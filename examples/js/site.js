let EventsBus = new Vue({
    methods: {
        deleteUserRow(index) {
            this.$emit('delete-user-row', index);
        }
    }
});

new Vue({
    el: "#app-container",
    data: {
        users: [
            {id: 1, fullName: "Jan Kowalski", age: 32},
            {id: 2, fullName: "Anna Nowak", age: 27},
            {id: 3, fullName: "Michal Gembicki", age: 32},
            {id: 4, fullName: "Robert Gilbert", age: 27},
            {id: 5, fullName: "Tomasz Roza", age: 32},
            {id: 6, fullName: "Anna Tar", age: 27},
        ],
        currentView: "users",
    },
    methods: {
        changeSection(name) {
            this.currentView = name;
        },
    },
    components: {
        'users': users,
        'home-page': homePage,
        'contact-page': contactPage,
        'nav-bar': navBar,
        'footer-elem': footer,
    },
    created() {
        EventsBus.$on('delete-user-row', userId => {
            let index = _.findIndex(this.users, ["id", userId]);
            this.users.splice(index, 1);
        });
    },
});