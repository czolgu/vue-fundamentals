let userRow = Vue.component('user-row', {
    template: `
        <tr>
            <td>{{ index + 1 }}</td>
            <template v-if="editMode === true">
                <td>
                    <div class="form-group">
                        <input type="text" class="form-input" v-model.lazy="user.fullName" />
                    </div>
                </td>
            </template>
            <template v-else>
                <td>{{ user.fullName }}</td>
            </template>
            <td>{{ user.age }}</td>
            <td>
                <button class="btn btn-primary" v-on:click="editMode = !editMode">
                    {{ editMode === true ? "Done" : "Edit" }}
                </button>
            </td>
            <td>
                <button v-on:click="deleteUserRow(user.id)" class="btn btn-error">
                    Delete
                </button>
            </td>
        </tr>    
    `,
    props: {
        user: {
            type: Object,
            required: true,
        },
        index: {
            required: true,
        },
    },
    data() {
        return {
            editMode: false,
        }
    },
    methods: {
        deleteUserRow(userId) {
            EventsBus.deleteUserRow(userId);
        },
    },
});
