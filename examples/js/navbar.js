let navBar = Vue.component('nav-bar', {
    template: `
        <header>
            <section class="navbar-section">
                <a href="https://vuejs.org" class="navbar-brand mr-2">Vue.js</a>
                <a href="#" class="btn btn-link" v-on:click.prevent="changeSection('home-page')">Home</a>
                <a href="#" class="btn btn-link" v-on:click.prevent="changeSection('contact-page')">Contact</a>
                <a href="#" class="btn btn-link" v-on:click.prevent="changeSection('users')">Users</a>
            </section>
        </header>
    `,
    methods: {
        changeSection(sectionName){
            this.$emit('navbar-section-change', sectionName);
        }
    }
});