let footer = Vue.component('footer-elem', {
    template: `
        <div class="container grid-lg">
            <footer>
                <p>Copyright &copy; {{ new Date().getFullYear() }}</p>
            </footer>   
        </div>
    `,
})