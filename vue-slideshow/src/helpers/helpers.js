const preloadImage = url => {
    const image = document.createElement("img");

    let p = new Promise((resolve, reject) => {
        image.onload = () => resolve(url);
        image.onerror = () => reject(url);
    });

    image.src = url;

    return p;
}

export {
    preloadImage
}