import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App'
import './css/main.css'

import routes from './routes/Routes'

const router = new VueRouter({
  routes
});

Vue.use(VueRouter); // for Vue to know that it uses vue-router
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  data: {
    endpoint: 'http://code.eduweb.pl/kurs-vue/images/',
  },
  template: '<App v-bind:endpoint="endpoint"/>',
})
